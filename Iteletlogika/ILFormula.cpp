/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Iteletlogika
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.25
*
* @description  Implementation of ILFormula class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#include "ILFormula.h"
#include "ILFormula_AUX.h"
#include <algorithm>
#include <stdio.h>
#include <ctype.h>
#include <string>

ILFormula::ILFormula()
{
    formula = "";
}
ILFormula::ILFormula(std::string m_formula)
{
    std::string hiba_uzenet;
    if(iteletlogikai_formula(m_formula, hiba_uzenet))
    {
      formula = m_formula;
    }
    else
    {
      std::cout << "                         HIBAS KIFEJEZES       " << hiba_uzenet << std::endl;
    }
}
bool ILFormula::iteletlogikai_formula(std::string m_formula, std::string &hiba)
{
    //Zarojelezes helyessegenek vizsgalata
    int t_nyitott_zarojelek_szama = std::count(m_formula.begin(), m_formula.end(), '(');
    int t_zart_zarojelek_szama    = std::count(m_formula.begin(), m_formula.end(), ')');
    std::string szintaxis_hiba;

    if(t_nyitott_zarojelek_szama != t_zart_zarojelek_szama)
    {
      hiba = "Hiba(00): Nem logikai formula. Hibas zarojelezes.";
      return false;
    }

    //Reszhelyesseg vizsgalata vagyis
    //a reszcsoportok vizsgalata
    if(!reszlegesen_helyes(m_formula,0,m_formula.length()-1,szintaxis_hiba))
    {
      hiba = "Hiba(01): Nem logikai formula. Szintaxis hiba.(eredet: " + szintaxis_hiba + ")"; 
      return false;
    }
  return true;
}
int ILFormula::logikai_osszetettseg()
{
    int t_logikai_osszetettseg = 0;

    //Bejarjuk a kifejezes elemeit es ha muveletet talalunk
    //inkrementaljuk a logikai osszetettseget szamolo
    //valtozot.

    for(int i = 0; i < formula.length(); i++)
    {
      if(formula[i] == '!' || formula[i] == '|' || formula[i] == '&' || formula[i] == '=' || formula[i] == '>')
      {
        t_logikai_osszetettseg++;
      }
    }

    return t_logikai_osszetettseg;
}
void ILFormula::kiir_zarojelezett_alak()
{
    std::string t_formula = formula;
    
    //Vizsgaljuk, hogy kell - e tenni zarojelt a kifejezes elejere
    //es vegere(kulso zarojelek letjogosultsaganak vizsgalata)
    if(t_formula[0] == '(')
    {
      int nyitott_zarojelek_szama = 1;
      int csukott_zarojelek_szama = 0;
      int pos                     = 1;
      while(nyitott_zarojelek_szama != csukott_zarojelek_szama)
      {
        if(t_formula[pos] == '(')
        {
          nyitott_zarojelek_szama++;
        }
        if(t_formula[pos] == ')')
        {
          csukott_zarojelek_szama++;
        }
          pos++;
      }
      if(pos != t_formula.length())
      {
        t_formula.insert(0,"(");
        t_formula.insert(t_formula.length(),")");
      }
    }
    else
    {
      t_formula.insert(0,"(");
      t_formula.insert(t_formula.length(),")");
    }

    //Zarojelezzuk a kifejezest az operatorok prioritasanak
    //csokkeno sorrendjebe 
    zarojelez_operator_szerint(t_formula,'=');
    zarojelez_operator_szerint(t_formula,'>');
    zarojelez_operator_szerint(t_formula,'&');  
    zarojelez_operator_szerint(t_formula,'|');

    std::cout << t_formula;
}
void ILFormula::kiir_zarojelmentes_alak()
{
    std::string t_formula = formula;

    int bal_index = 0;
    int jobb_index = t_formula.length() - 1;
    
    //Vizsgaljuk, hogy ki lehet - e torolni az elso zarojelet
    //(vannak - e elhagyhato kulso zarojelek).
    //Megnezzuk, hogy az elso zarojel az utolsohoz tartozik
    //vagy sem.Ha hozza tartozik akkor kitoroljuk az elsot
    //es az utolsot is.
    if(t_formula[bal_index] == '(')
    {
      int nyitott = 1;
      int csukott = 0;
      int pos     = 1;
      while(nyitott!= csukott)
      {
        if(t_formula[pos] == '(')
        {
          nyitott++;
        }
        if(t_formula[pos] == ')')
        {
          csukott++;
        }
          pos++;
      }
      if(pos != t_formula.length())
      {
        t_formula.erase(0,1);
        t_formula.erase(t_formula.length()-1,1);
      }
    }
    
    //Bejarjuk a kifejezes osszes elemet
    for(int i = 0; i < t_formula.length(); i++)
    {
      //std::cout << t_formula[i]<<std::endl;

      //Ha kapunk egy nyitott zarojelet megkeressuk a parjat
      //majd utana megvizsgaljuk, hogy a zarojeles csoport
      //elotti vagy utani operator prioritas milyen a belso
      //operatorok prioritasahoz kepest.Ha a kulsok prioritasa
      //nagyobb vagy egyenlo ??? a belsokevel akkor a zarojelpar
      //nem elhagyhato kulonben pedig igen.
      if(t_formula[i] == '(')
      {
        bal_index                   = i;
        jobb_index                  = i + 1;
        int nyitott_zarojelek_szama = 1;
        int csukott_zarojelek_szama = 0;

        while(nyitott_zarojelek_szama != csukott_zarojelek_szama)
        {
          if(t_formula[jobb_index] == '(')
          {
            nyitott_zarojelek_szama++;
          }
          if(t_formula[jobb_index] == ')')
          {
            csukott_zarojelek_szama++;
          }
          jobb_index++;
        }
        jobb_index--;
    
        int zarojel_max_operator_prioritas = 5;
        int zarojelen_kivuli_max_operator_prioritas = 0;


        for(int pos = bal_index ; pos < jobb_index; pos++)
        {
          if(t_formula[pos] == '=' || t_formula[pos] == '>' || t_formula[pos] == '&' || t_formula[pos] == '|')
          {
            if(prioritas_ertek(t_formula[pos]) < zarojel_max_operator_prioritas)
            {
              zarojel_max_operator_prioritas = prioritas_ertek(t_formula[pos]);
            }
          }
        } 
       //std::cout << t_formula[bal_index-1] << " " << t_formula[jobb_index+1] << std::endl;
        if(prioritas_ertek(t_formula[bal_index - 1]) > prioritas_ertek(t_formula[jobb_index + 1]))
        { 
          zarojelen_kivuli_max_operator_prioritas = prioritas_ertek(t_formula[bal_index - 1]);
        }
        else
        {
          zarojelen_kivuli_max_operator_prioritas = prioritas_ertek(t_formula[jobb_index + 1]);
        }
        
        //std::cout << zarojelen_kivuli_max_operator_prioritas << " " << zarojel_max_operator_prioritas << std::endl;
        if(zarojel_max_operator_prioritas > zarojelen_kivuli_max_operator_prioritas)
        {
          t_formula.erase(bal_index,1);
          jobb_index--;
          t_formula.erase(jobb_index,1);
          i--;
          
        }
        else
        {
          continue;
        }
      }
    }

    std::cout << t_formula;
}
void ILFormula::kiir_osszes_reszformula()
{
    std::string t_formula = formula;

    //Bejarjuk a kifejezes osszes elemet
    for(int i = 0; i < t_formula.length(); i++)
    { 
      
      //Ha '!' kapunk kiirjuk ot es az utana kovetkezo
      //elemet vagy zarojeles csoportot.
      if(t_formula[i] == '!')
      {
        int j = i;
        std::cout << t_formula[j];
        j++;

        if(t_formula[j] == '(')
        {
          std::cout << t_formula[j];
          int nyitott_zarojelek_szama = 1;
          int csukott_zarojelek_szama = 0;

          while(nyitott_zarojelek_szama != csukott_zarojelek_szama)
          {
            j++;
            if(t_formula[j] == '(')
            {
              nyitott_zarojelek_szama++;
            }
            if(t_formula[j] == ')')
            {
              csukott_zarojelek_szama++;
            }
            std::cout << t_formula[j];
            
         }
        }
        else
        {
          std::cout <<  t_formula[j];;
        }
        std::cout << "  ";
      }
      //Ha nagybetut(predikatumszimbolumot) kapunk kiirjuk.
      if(isupper(t_formula[i]) || isdigit(t_formula[i]))
      {
        int j = i;
        std::cout << t_formula[j];
        std::cout << "  ";
      }
      //Ha '(' kapunk kiirjuk az utana kovetkezo zarojeles
      //csoportot.
      if(t_formula[i] == '(')
      {   
          int j = i;

          std::cout << t_formula[j];
          int nyitott = 1;
          int csukott = 0;

          while(nyitott != csukott)
          {
            j++;
            if(t_formula[j] == '(')
            {
              nyitott++;
            }
            if(t_formula[j] == ')')
            {
              csukott++;
            }
            std::cout << t_formula[j];
         }
         std::cout << "  ";
      }
    }
}




