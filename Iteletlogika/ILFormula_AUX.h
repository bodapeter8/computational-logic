/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Iteletlogika
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.25
*
* @description  Auxiliary header for ILFormula class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/
#pragma once

#include <iostream>
#include <string>


bool reszlegesen_helyes(std::string m_formula,int kezdo_index,int vegso_index, std::string &hiba)
{
    //Kifejezes helyessegenek vizsgalata rekurzivan.
    //A kifejezest szakaszokra bontjuk es vizsgaljuk rekurzivan 
    //a helyesseguket.

    if(kezdo_index > vegso_index)
    {
      hiba = "Hiba(02): Nem logikai formula. Hibas kifejezes.";
      return false;
    }
    int t_utolso = kezdo_index + 1;
    
    //Elso karakter helyessegenek vizsgalata
    if(!isupper(m_formula[kezdo_index]) && !isdigit(m_formula[kezdo_index]) && m_formula[kezdo_index] != '!' && m_formula[kezdo_index] != '(')
    {
      hiba = "Hiba(03): Nem logikai formula. Hibas kifejezes.";
      return false;
    }
    
    //reszkifejezes kivalasztasa
    if(m_formula[kezdo_index] == '(')
    {
      int t_nyitott_zarojelek_szama = 1;
      int t_zart_zarojelek_szama    = 0;
      int t_index                   = kezdo_index;
      

      while(t_nyitott_zarojelek_szama != t_zart_zarojelek_szama)
      {
        t_index++;
        if(m_formula[t_index] == '(')
        {
          t_nyitott_zarojelek_szama++;
        }
        if(m_formula[t_index] == ')')
        {
          t_zart_zarojelek_szama++;
        }
        
      }

      if(!reszlegesen_helyes(m_formula, kezdo_index+1, t_index - 1, hiba))
      {
        hiba = "Hiba(04): Nem logikai formula. Hibas kifejezes.";
        return false;
      }
      else
      {
        t_utolso = t_index + 1;
      }
    }
    while(t_utolso <= vegso_index)
    {
       if(isupper(m_formula[t_utolso]) || isdigit(m_formula[t_utolso]) || m_formula[t_utolso] == '!')
       {
         if(m_formula[t_utolso - 1]    == '!' 
            || m_formula[t_utolso - 1] == '&'
            || m_formula[t_utolso - 1] == '|' 
            || m_formula[t_utolso - 1] == '>'
            || m_formula[t_utolso - 1] == '=')
         {
           t_utolso++;
         }
         else
         {
           hiba = "Hiba(05): Nem logikai formula. Hibas kifejezes.";
           return false;
         }
       }
       else if(m_formula[t_utolso]   == '&'
              || m_formula[t_utolso] == '|' 
              || m_formula[t_utolso] == '>'
              || m_formula[t_utolso] == '=')
        {
          if(isupper(m_formula[t_utolso - 1]) || isdigit(m_formula[t_utolso - 1]) || m_formula[t_utolso - 1] == ')')
          {
            t_utolso++;
          }
          else
          { 
            hiba = "Hiba(06): Nem logikai formula. Hibas kifejezes.";
            return false;
          }
        }
       else if(m_formula[t_utolso] == '(')
       {
         if(m_formula[t_utolso - 1]    != '!' 
           && m_formula[t_utolso - 1]  != '&'
           && m_formula[t_utolso - 1]  != '|' 
           && m_formula[t_utolso - 1]  != '>'
           && m_formula[t_utolso - 1]  != '=')
         {
           hiba = "Hiba(07): Nem logikai formula. Hibas kifejezes.";
           return false;
         }
         int t_nyitott_zarojelek_szama_u = 1;
         int t_zart_zarojelek_szama_u    = 0;
         int t_index_u                   = t_utolso;
      

         while(t_nyitott_zarojelek_szama_u != t_zart_zarojelek_szama_u)
         {
           t_index_u++;
           if(m_formula[t_index_u] == '(')
           {
             t_nyitott_zarojelek_szama_u++;
           }  
           if(m_formula[t_index_u] == ')')
           {
             t_zart_zarojelek_szama_u++;
           }
         }
         if(!reszlegesen_helyes(m_formula,t_utolso + 1,t_index_u - 1,hiba))
         {
           hiba = "Hiba(08): Nem logikai formula. Hibas kifejezes.";
           return false;
         }
         else
         {
           t_utolso = t_index_u + 1;
         }
      }
    }

    //Utolso karakter helyessegenek vizsgalata
    if(m_formula[vegso_index]    == '&'
       || m_formula[vegso_index] == '|' 
       || m_formula[vegso_index] == '>'
       || m_formula[vegso_index] == '=')
    {
      hiba = "Hiba(09): Nem logikai formula. Hibas kifejezes.";
      return false;
    }
  return true;
}
void zarojelez_operator_szerint(std::string &m_formula,char oper)
{
    
    for(int pos = 0; pos < m_formula.length(); pos++)
    {
      
      if(m_formula[pos] == oper)
      {
        int pos_op_balra = pos - 1;
        bool balra_insert = true;
        bool balra_zarojelez = true;
        //Zarojelezzuk az operator elotti csoportot
        if(m_formula[pos - 2] == '!' && m_formula[pos - 3] == '(')      
        {
          balra_insert = false;
        }
        if(m_formula[pos - 2] != '(')
        {
          balra_insert = true && balra_insert;
        }
        else
        {
          balra_insert = false;
        }
        
        int pos_op_elott = pos - 1;
        while(m_formula[pos_op_elott] != '(' && pos_op_elott != 0)
        {
          pos_op_elott--;
        }
        //std::cout << m_formula[pos_op_balra] << m_formula[pos_op_elott];
        if((m_formula[pos_op_balra] == ')' && m_formula[pos_op_elott] == '(') || balra_insert == false)
        {
          balra_zarojelez = false;
        }
        else
        {
          m_formula.insert(pos_op_balra + 1,")");
          pos++;
          m_formula.insert(pos_op_elott,"(");
          pos++;
        }
         

        int pos_op_jobbra = pos + 1;
        bool jobbra_insert = false;
        //Zarojelezzuk az operator utani csoportot
        if(m_formula[pos + 1] == '!' && m_formula[pos + 3] == ')')
        {
          continue;
        }
        if(m_formula[pos + 2] != ')')
        {
          jobbra_insert = true;
        }
        else
        {
          continue;
        }
        int pos_op_utan = pos + 1;
        while(m_formula[pos_op_utan] != ')' && pos_op_utan != m_formula.length())
        {
          pos_op_utan++;
        }
        if((m_formula[pos_op_jobbra] == '(' && m_formula[pos_op_utan] == ')') || jobbra_insert == false)
        {
          continue;
        }
        else
        {
          m_formula.insert(pos_op_jobbra,"(");
          pos_op_utan++;
          m_formula.insert(pos_op_utan,")");
        }
      }
    }
    
}
int prioritas_ertek(char oper)
{
    switch(oper)
    {
      case '=' : return 1;
      case '>' : return 2;
      case '&' : return 3;
      case '|' : return 3;
      case '!' : return 4;
    }
    return 0;
}