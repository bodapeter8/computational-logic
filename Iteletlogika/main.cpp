/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Iteletlogika
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.25
*
* @description  Main
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/
#include "ILFormula.h"
#include <iostream>
#include <iomanip>
#include <fstream>

int main()
{ 
    std::ofstream output_file;
    output_file.open("output.dat");
    std::ifstream input_file;
    input_file.open("input.dat");

    std::cout.rdbuf(output_file.rdbuf());
    std::string input_string;
    std::string error;
    std::string error_log;
    int line_number = 0;
    

    while(!input_file.eof())
    {
      line_number++;
      input_file >> input_string;
      std::cout << "FORMULA: " << "                " << input_string << std::endl;  
      
      ILFormula formula(input_string);
      
      
      if(formula.iteletlogikai_formula(input_string, error))
      {
        std::cout << "ZAROJELEZETT ALAK:       ";  
        formula.kiir_zarojelezett_alak();
        std::cout << std::endl;
        std::cout << "ZAROJEL MENTES ALAK:     ";
        formula.kiir_zarojelmentes_alak();
        std::cout << std::endl;
        std::cout << "OSSZES RESZFORMULA:      ";
        formula.kiir_osszes_reszformula();
        std::cout << std::endl;
        std::cout << "LOGIKAI OSSZETETTSEG =   ";
        std::cout << formula.logikai_osszetettseg();
        std::cout << std::endl;
        std::cout << std::endl;
      }
      else
      {
        error_log += "Line " + std::to_string(line_number) + ' ' + error + '\n';
        std::cout << std::endl;
      }
    }

    std::cout << "ERROR_LOG:" << std::endl;
    std::cout << error_log;
    return 0;

}
