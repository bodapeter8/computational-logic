
/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Iteletlogika
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.25
*
* @description  Definition of ILFormula class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#pragma once

#include <iostream>

class ILFormula{

private:
    
    /**
     * @brief          Tárolt formula.
     * 
     */
    std::string formula;

public:
    
    ILFormula();
    ILFormula(std::string);
    /**
     * @brief          Eldönti, hogy egy beolvasott szimbólumsorozat
     *                 lehet-e ítéletlogikai formula.
     * @return true    Ha lehet ítéletlogikai formula.
     * @return false   Ha lehet ítéletlogikai formula.Kiirja, hogy miert nem lehet.
     */
    bool iteletlogikai_formula(std::string, std::string &hiba);
    /**
     * @brief          Kiírja a tárolt formula teljesen zárójelezett alakját.
     * 
     */
    void kiir_zarojelezett_alak();
    /**
     * @brief          Kiírja a tárolt formulát a lehető legtöbb zárójelpárt elhagyva belőle.
     * 
     */
    void kiir_zarojelmentes_alak();
    /**
     * @brief          Meghatározza a tárolt formula logikai összetettségét.
     * 
     * @return int     Tárolt formula logikai összetettsége.
     */
    int logikai_osszetettseg();
    /**
     * @brief          Megkeresi es kiirja a tárolt formula összes részformuláját.
     * 
     */
    void kiir_osszes_reszformula();

    inline std::string get_formula()
    {
      return formula;    
    }
    inline void set_formula(std::string m_formula)
    {
      formula = m_formula;
    }
};

