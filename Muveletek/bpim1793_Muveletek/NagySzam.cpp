/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Muveletek
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.11
*
* @description  Implementation of NagySzam class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#include "NagySzam.h"
#include <iostream>

NagySzam::NagySzam(){};
NagySzam::NagySzam(const NagySzam &x)
{
  elojel      = x.elojel;
  szam_hossza = x.szam_hossza;
  szamjegyek  = new int[szam_hossza];

  for(int i = 0 ; i < szam_hossza ; i++){
    szamjegyek[i] = x.szamjegyek[i];
  } 
}
NagySzam::NagySzam(std::string szam)
{
  if(szam[0] == '-'){
      elojel      = -1;
      szam_hossza = szam.length() - 1;
      szamjegyek  = new int[szam_hossza];

      for(int i=1; i <= szam_hossza; i++){
          szamjegyek[i-1] = szam[i] - '0';
      }
  }
  else{
      elojel      = 1;
      szam_hossza = szam.length();
      szamjegyek  = new int[szam_hossza];

      for(int i=0; i < szam_hossza; i++){
          szamjegyek[i] = szam[i] - '0';
      }

  }
}



