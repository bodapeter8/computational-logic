/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Muveletek
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.11
*
* @description  Implementation of NagyEgesz class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#include <iostream>
#include "NagyEgesz.h"
using namespace std;

NagyEgesz::NagyEgesz(){
	this->n = 1;
	this->elojel = 1;
	v = new int[1];
	v[0] = 0;
}
NagyEgesz::NagyEgesz(int elojel, int n, const int* szamjegyek) {
	this->elojel = elojel;
	this->n = n;
	v = new int[n+5];
	for (int i = 0; i < n; i++) {
		v[i] = szamjegyek[i];
	}
	
}
NagyEgesz::NagyEgesz(const NagyEgesz &x) {

	
	this->n = x.n; 
	v = new int[n + 5];
	this->elojel = x.elojel;
	for (int i = 0; i < n; i++) {
		v[i] = x.v[i];
	}
	
	
}

void NagyEgesz::kiir() const{

	bool mindNulla = true;
	int j = 0;

    for(int k =0 ; k < n ; k++){
		if(v[k] != 0){
			mindNulla = false;
			break;
		}
	}

	if(!mindNulla){

	if (elojel == -1) {

		if(v[0] == 0 && n == 1){cout << "0" ;}
		else{cout << "-";}	
	}
	
	
	while(v[j] == 0){
		j++;
	}
	for (int i = j; i < n; i++) {
		
		cout << v[i];
	}
	}
	else{
		cout << "0";
	}
	
	cout << endl;
}

void NagyEgesz::eltolJobbra(int k) {
	this->n = this->n + k;

	for (int i = n-1; i >= k ; i--) {
		v[i] = v[i - k];
	}
	for (int j = 0; j < k; j++) {
		v[j] = 0;
	}
}
NagyEgesz NagyEgesz::osszead(const NagyEgesz &x) {

	
	NagyEgesz A(*this);
	NagyEgesz B = x;
	int hosszabbSzamMerete = 0;
	int hosszKulonbseg = 0;

	if (A.elojel == 1 && B.elojel == 1 || A.elojel == -1 && B.elojel == -1){
		NagyEgesz E1;

		if (A.n > B.n) {
			hosszabbSzamMerete = A.n;
			hosszKulonbseg = A.n - B.n;
			B.eltolJobbra(hosszKulonbseg);
			//szoB.kiir();
			
		}
		else {
			hosszabbSzamMerete = B.n;
			hosszKulonbseg = B.n - A.n;
			A.eltolJobbra(hosszKulonbseg);
		}

		int maradek = 0;
		E1.n = hosszabbSzamMerete;
		delete[] E1.v;
		E1.v = new int[hosszabbSzamMerete + 5];
		for (int i = 0; i < hosszabbSzamMerete; i++) {
			E1.v[i] = 0;
		}
		
		for (int i = hosszabbSzamMerete-1; i >= 0; i--) {
			E1.v[i] = (A.v[i] + B.v[i] + maradek) % 10;
			maradek = (A.v[i] + B.v[i] + maradek) / 10;
		}
		
		if (maradek != 0) {
			E1.eltolJobbra(1);
			E1.v[0] = maradek;
		}
		if (A.elojel == -1) {
			E1.elojel = -1;
		}

		
			
		return E1;
	}
	else {

		if (A.elojel == 1 && B.elojel == -1) {

			B.elojel = 1;
			NagyEgesz E2(A.kivon(B));
			return E2;
		}
		if (A.elojel == -1 && B.elojel == 1) {

			A.elojel = 1;
			NagyEgesz E3(B.kivon(A));
			return E3;
		}
		

	}
}
NagyEgesz NagyEgesz::kivon(const NagyEgesz &x){
	NagyEgesz C(*this);
	NagyEgesz D(x);

	

	

	int hosszabbSzamMerete = 0;
	int hosszKulonbseg = 0;
	bool CNagyobb = false;
	bool voltCserelve = false;

	if (C.elojel == 1 && D.elojel == 1) {

		NagyEgesz E1;

		if (C.n >= D.n) {
			hosszabbSzamMerete = C.n;
			hosszKulonbseg = C.n - D.n;
			D.eltolJobbra(hosszKulonbseg);
			CNagyobb = true;
			
		}
		else {
			hosszabbSzamMerete = D.n;
			hosszKulonbseg = D.n - C.n;
			C.eltolJobbra(hosszKulonbseg);
		}
		for(int i = 0 ; i < hosszabbSzamMerete ; i++){
			
			if(C.v[i] < D.v[i]){

				voltCserelve = true;
				
				
			}
			else if(C.v[i] > D.v[i]){
				break;
			}
		}

		int maradek = 0;
		E1.n = hosszabbSzamMerete;
		E1.v = new int[hosszabbSzamMerete + 5];
		for (int i = 0; i < hosszabbSzamMerete; i++) {
			E1.v[i] = 0;
		}
		if(!voltCserelve){
		for (int i = hosszabbSzamMerete - 1; i >= 0; i--) {

			

			if (C.v[i] + maradek >= D.v[i]) {
				E1.v[i] = C.v[i] + maradek - D.v[i];
				maradek = 0;
			}
			else {
				E1.v[i] = C.v[i] + maradek + 10 - D.v[i];
				maradek = -1;
			}
		}
		}
		else{
			for (int i = hosszabbSzamMerete - 1; i >= 0; i--) {

			

			if (D.v[i] + maradek >= C.v[i]) {
				E1.v[i] = D.v[i] + maradek - C.v[i];
				
				maradek = 0;
			}
			else {
				E1.v[i] = D.v[i] + maradek + 10 - C.v[i];
				
				maradek = -1;
			}
		}

		
		}
		
	
		if (!CNagyobb) {
			E1.elojel = -1;
		}
		if(voltCserelve){
			E1.elojel = -1;
		}

		return E1;
	}
	else {
		if (C.elojel == 1 && D.elojel == -1) {

			D.elojel = 1;
			NagyEgesz E2(C.osszead(D));
			
			return E2;
		}
		if (C.elojel == -1 && D.elojel == 1) {

			D.elojel = -1;
			NagyEgesz E3(C.osszead(D));
			return E3;
		}
		if (C.elojel == -1 && D.elojel == -1) {

			D.elojel = 1;
			C.elojel = 1;
			NagyEgesz E4(D.kivon(C));
			return E4;
		}
	}
}
int szamjegyek(int k) {
	k = k > 0 ? k : -k;
	int sum = 0;
	while (k != 0) {
		++sum;
		k /= 10;
	}
	return sum;
}

NagyEgesz NagyEgesz::szamjegySzoroz(int szamjegy) {
	NagyEgesz szorzat;
	//delete  [] szorzat.v;
	szorzat.v = new int[n + 1];
	szorzat.n = n;
	int maradek=0;
	for (int i = n - 1; i >= 0; i--) {
		szorzat.v[i] = (szamjegy * v[i] + maradek)%10;
		maradek = (szamjegy * v[i] + maradek) / 10;
	}
	if (maradek) {
		szorzat.eltolJobbra(1);
		szorzat.v[0] = maradek;
	}
		
	
	return szorzat;
}
void  NagyEgesz::eltolBalra(int k) {

	int* uj = new int[n+k];
	for (int i = 0; i < n; i++) {
		uj[i] = v[i];
	}
	delete[] v;
	v = uj;
	for (int i = n; i < n + k; i++) {
		v[i] = 0;
	}
	n = n + k;


}
NagyEgesz NagyEgesz::szoroz(const NagyEgesz &x) {

	NagyEgesz szum;
	NagyEgesz szorzat;
		

	int tolas = 0;
	for (int i = x.n - 1; i >= 0; i--) {
		szorzat = (*this).szamjegySzoroz(x.v[i]);
		szorzat.eltolBalra(tolas);
		tolas++;
		szum = szum.osszead(szorzat);
		
	}
	if (elojel == -1 && x.elojel == 1 || elojel == 1 && x.elojel == -1)szum.elojel = -1;
	
	return szum;

}
NagyEgesz NagyEgesz::oszt(const NagyEgesz& b) {
    if(b.v[0] == 0) {
        throw NullavalValoOsztas();
    }
    int* aux1 = new int [n];
    int* aux2 = new int [n];
    for(int i = 0; i < n; i++) {
        aux1[i] = v[i];
        aux2[i] = 0;
    }
    int i = 0, j, k;
    /*az elso szam legkisebb olyan reszeenek 'eleje' es 'vege', amelyik nagyobb mint a masodik*/
    int veg = b.n;
    int eleje = 0;
    int q = 0;
    char s;
    if(n < b.n){
		
        return NagyEgesz();
    }else{
        if(n == b.n){
            for(int i = 0; i < n; i++){
                if(v[i] < b.v[i]){
					    return NagyEgesz();
                }
				else{
					break;
				}
            }
        }

    }

    while(veg <= n) {//amig nincs elosztva

	
        bool ok = false;
        int lehozott = 0;
        while(!ok && veg <= n) {

            eleje = i;
            if(veg-eleje == b.n) { //ha az osztandobol levalasztott resz ugyanolyan hosszu mint az oszto
                for(j = eleje, k = 0; k < b.n; j++, k++) {
                    if(aux1[j] < b.v[k]) { //ha ez a szam kisebb, akkor a levalasztott reszt novelem

                        veg++;
                        lehozott++;
                        ok = true;
                        break;
                    }else{
                        if(aux1[j] > b.v[k]){// ha megfelel a levalasztott resz
                            ok = true;
                            break;
                        }
                    }
                }
                if(j == veg && !ok ) {
                    ok = true;
                }

            } else {
                if(veg-eleje < b.n) {
                    veg++;
                    lehozott++;
                }
            }
            if(lehozott == 2) {
                aux2[q] = 0; //ha legalabb 2 szamot kell lehozni, akkor a hanyadosba bekerul egy nulla

                q++;
            }

        }
        for(i = eleje; i < veg; i++) {
            if(aux1[i] != 0) break;
        }
        bool ok2 = false;
        if(aux1[eleje] == 0) {
            ok2 = true;
            for(i = eleje+1; i < veg; i++) {
                aux2[q] = 0;
                q++;
            }
        }

        if(veg > n){ok2 = true;}
        while(!ok2) {
            i = veg-1, j = b.n-1, k = eleje;
            int kolcson = 0;
            bool ok3 = false;
            while(i >= k && j >= 0) {//ismetelt kivonasal elsztom a levalasztott szambol az osztot

                aux1[i] -= (b.v[j] + kolcson);

                if(aux1[i] < 0) {
                    aux1[i] += 10;
                    kolcson = 1;
                } else {
                    kolcson = 0;
                }
                i--;
                j--;
                if(!ok3) ok3 = true;
            }

            aux1[i] -= kolcson;
            if(ok3) {
                aux2[q]++;
            }


            for(i = eleje; i < veg; i++) {
                if(aux1[i] != 0) break;
            }
            eleje = i;
            if(veg - eleje < b.n) { //ha a lavalasztott szam kissebb mint az oszto, akkor lehozzuk a kovetkezo szamjegyeket majd
                ok2 = true;
            } else {
                if(veg - eleje == b.n)
                    for(j = 0, k = eleje; k < veg; j++, k++) {
                        if(aux1[k] > b.v[j]) {

                            break;
                        }else{
                            if(aux1[k] < b.v[j]){
                                ok2 = true;
                                break;
                            }
                        }
                    }
            }
            for(i = eleje; i < veg; i++) {
                if(aux1[i] != 0) break;
            }
            eleje = i;

        }
        q++;

    }
	
    q--;
	
    int elojeleredmeny = elojel * b.elojel;
    NagyEgesz aux(elojeleredmeny, q, aux2);
	
	
    delete [] aux1;
    delete [] aux2;
    return aux;

}
