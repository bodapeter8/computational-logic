/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Muveletek
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.11
*
* @description  Definition of NagySzam class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#pragma once

#include <iostream>

class NagySzam{
  private:
    int elojel;
    int szam_hossza;
    int* szamjegyek;
  public:
    NagySzam();
    NagySzam(const NagySzam &x);
    NagySzam(std::string szam);
    NagySzam& operator=(const NagySzam&x)
    {
		elojel       = x.elojel;
        szam_hossza  = x.szam_hossza;
        szamjegyek   = new int[szam_hossza];

      for(int i = 0 ; i < szam_hossza ; i++)
      {
        szamjegyek[i] = x.szamjegyek[i];
      }   
	  return *this;
    }
    inline int get_elojel(){
        return elojel;
    }
    inline int get_szam_hossza(){
        return szam_hossza;
    }
    inline int* get_szamjegyek(){
        return szamjegyek;
    }
};