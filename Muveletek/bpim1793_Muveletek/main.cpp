/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Muveletek
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.11.11
*
* @description  Main
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#include <iostream>
#include <fstream>
#include "NagySzam.h"
#include "NagyEgesz.h"

int main()
{
  ofstream output_file;
  output_file.open("output.dat");
  ifstream input_file;
  input_file.open("input.dat");

  std::string t_elso_szam_string = "";
  std::string t_masodik_szam_string = ""; 
  std::string t_muvelet;

  std::cout.rdbuf(output_file.rdbuf());

  while(!input_file.eof())
  {
    input_file >> t_elso_szam_string;
    input_file >> t_masodik_szam_string;
    input_file >> t_muvelet;

    NagySzam t_nagyszam_elso(t_elso_szam_string);
    NagySzam t_nagyszam_masodik(t_masodik_szam_string);

    NagyEgesz t_elso_szam(t_nagyszam_elso.get_elojel(),t_nagyszam_elso.get_szam_hossza(),t_nagyszam_elso.get_szamjegyek());
    NagyEgesz t_masodik_szam(t_nagyszam_masodik.get_elojel(),t_nagyszam_masodik.get_szam_hossza(),t_nagyszam_masodik.get_szamjegyek());
    
    switch(t_muvelet[0])
    {
      case '+' :
      {
        NagyEgesz t_eredmeny(t_elso_szam.osszead(t_masodik_szam));
        t_eredmeny.kiir(); 
        break;  
      }
      case '-' :
      {
        NagyEgesz t_eredmeny(t_elso_szam.kivon(t_masodik_szam));
        t_eredmeny.kiir();  
        break;  
      }
      case '*' :
      {
        NagyEgesz t_eredmeny(t_elso_szam.szoroz(t_masodik_szam));
        t_eredmeny.kiir();
        break;    
      }
      case '/' :
      {
        NagyEgesz t_eredmeny(t_elso_szam.oszt(t_masodik_szam));
        t_eredmeny.kiir();    
        break;
      }
    }
  }

  
  return 0;    
}
