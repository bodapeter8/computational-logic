/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Szamrendszer
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.10.26
*
* @description  Implementation of FileRead class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#define DEFAULT_PRECISION 5

//include libraries
#include <iostream>

class Conversion{
    
    private:
      
      int base_from;
      int base_to;
      int precision;
      std::string number;

    public:
      Conversion()
      {
         base_from  = 2;
         base_to    = 10;
         number     = 10;
         precision  = DEFAULT_PRECISION;
      }
      Conversion(int m_base_from, int m_base_to, std::string m_number, int m_precision = DEFAULT_PRECISION )
      {
        if(m_base_from < 1 || m_base_from > 16 ||
           m_base_to < 1 || m_base_to > 16)
        {
                throw("IID_WB");
        }
        
        for(int i = 0; i < m_number.length(); i++)
        {   
            
            switch(m_number[i])
            {
              
              case '0'...'9':
              if((m_number[i]-'0') >= m_base_from)
              {
                  throw("IID_WD");  
              }
              break;

              case 'A'...'F':
              if((m_number[i]-'7') >= m_base_from)
              {
                 throw("IID_WD");             
              }
              break;
              
              case ',':   
              break;

              default:
              {
                 throw("IID_UC");
              }  
            }
        }
        
            base_from  = m_base_from;
            base_to    = m_base_to;
            number     = m_number;
            precision  = m_precision;
                                      
      }


      //********************************************
      //* Converting to base 10
      //********************************************
      void convert_to_10(unsigned long &result_integer, double &result_decimal)
      {   
        //********************************************
        //* Converting the integer part of the number
        //********************************************
        
        std::string number_integer = number.substr(0,number.find(','));
        unsigned long t_result     = 0;
        int t_power                = 1;
        int t_digit                = 0;
        int t_length               = number_integer.size() - 1;
        
        
        //from the last character to the first charecter  
        for(int i = t_length; i >= 0; --i)
        {     
            
            //if it is a digit
            if(number_integer[i] >= '0' && number_integer[i] <= '9')
            {
              t_digit   = number_integer[i] - '0';  //char to int
              t_result += t_digit * t_power;
              t_power   = t_power * base_from;
            }  
            //if it is a letter
            else
            {
              t_digit   = number_integer[i] - '7'; //char to int
              t_result += t_digit * t_power;
              t_power   = t_power * base_from;  
            }    
        }
        result_integer = t_result;
        
          
        //********************************************
        //* Converting the decimal part of the number
        //********************************************

        std::string number_decimal = number.substr(number.find(',')+1,number.size()-number.find(',') - 1);
        double t_result_decimal   = 0;
        t_digit                   = 0;
        t_length                  = number_decimal.size() - 1;
        
        if(number.find(',') != std::string::npos)
        {
          //from the last character to the first character  
          for(int i = t_length; i >= 0; --i)
          {     
              
              //if it is a digit
              if(number_decimal[i] >= '0' && number_decimal[i] <= '9')
              {
                  t_digit           = number_decimal[i] - '0';  //char to int
                t_result_decimal  = t_result_decimal / base_from + t_digit ;
              }
               //if it is a letter
              else
              {
                t_digit           = number_decimal[i] - '7'; //char to int
                t_result_decimal  = t_result_decimal / base_from + t_digit ;
              }
               
          }
          
          result_decimal = t_result_decimal / base_from;
        }
        else
        {
          result_decimal = 0;
        }
      }


      //********************************************
      //* Converting to base base_to
      //********************************************
      std::string convert()
      {
        std::string t_integer_string  = "";
        std::string t_decimal_string  = "";
        std::string t_result_string   = "";
        int t_integer_digit           = 0;
        double t_decimal_digit        = 0;
        int t_precision               = precision;
        unsigned long t_integer       = 0;
        double t_decimal              = 0;
        convert_to_10(t_integer,t_decimal);

        //********************************************
        //* Converting the integer part of the number
        //********************************************

        while(t_integer)
        {
          t_integer_digit = t_integer % base_to;

          //if it is a digit
          if(t_integer_digit >= 0 && t_integer_digit <= 9)
          {
            t_integer_string = char('0' + t_integer_digit) + t_integer_string;
          }

          //if it is a letter
          else
          {
            t_integer_string = char('7' + t_integer_digit) + t_integer_string;
          }

          t_integer = t_integer / base_to;
        }

        //********************************************
        //* Converting the decimal part of the number
        //********************************************

        int t_precision_iterator = 1;

        while(t_decimal && t_precision_iterator <= t_precision )
        {
          t_decimal       = t_decimal*base_to;
          t_decimal_digit = int(t_decimal);

          //if it is a digit
          if(t_decimal_digit >= 0 && t_decimal_digit <= 9)
          {
            t_decimal_string = t_decimal_string + char('0' + t_decimal_digit);
          }

          //if it is a letter
          else
          {
            t_decimal_string = t_decimal_string + char('7' + t_decimal_digit);
          }

          t_decimal = t_decimal - t_decimal_digit;
          t_precision_iterator++;
        }

        if(t_integer_string == "" && t_decimal_string != "")
        {
          return "0," + t_decimal_string;
        }
        if(t_integer_string != "" && t_decimal_string == "")
        {
          return t_integer_string;
        }
        if(t_integer_string != "" && t_decimal_string != "")
        {
          return t_integer_string + "," + t_decimal_string;
        }
        if(t_integer_string == "" && t_decimal_string == "")
        {
          return "0";
        }
      }
};    