#pragma once
#include "NagySzam.h"

class NagyMuvelet{
  private:
    NagySzam szam_1;
    NagySzam szam_2;
    std::string muvelet;
  public:
    NagyMuvelet(){}
    NagyMuvelet(NagySzam s1, NagySzam s2, std::string muv){
        szam_1   = s1;
        szam_2   = s2;
        muvelet  = muv;
    }
    
};