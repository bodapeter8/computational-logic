#include <iostream>
using namespace std;


class NagyEgesz{

	int *v;
	int n;
	int elojel;
	
public:

	NagyEgesz();
	NagyEgesz(int elojel, int n, const int* szamjegyek);
	NagyEgesz(const NagyEgesz &x);
	~NagyEgesz() {
		
	 delete  []  v;
	}
	NagyEgesz& operator=(const NagyEgesz&x) {
		n = x.n;
		delete [] v;
		v = new int[n];
		for (int i = 0; i < n; i++) {
			v[i] = x.v[i];
		}
		return *this;
	}
	
	void kiir ()const;
	void eltolJobbra(int k);
	void eltolBalra(int k);
	NagyEgesz osszead(const NagyEgesz &x);
	NagyEgesz kivon(const NagyEgesz &x);
	NagyEgesz szamjegySzoroz(int szamjegy);
	NagyEgesz szoroz(const NagyEgesz &x);
	class NullavalValoOsztas{};
	NagyEgesz oszt(const NagyEgesz &x);

		
};