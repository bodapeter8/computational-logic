#pragma once

#include <iostream>
#include <fstream>

class FileIO{
  private:
    std::string path_to_file;
    char open_mode;
    int line_pointer;
  public:
    FileIO();
    FileIO(std::string m_path_to_file,char m_open_mode = 'r');

    inline std::string get_path_to_file()
    {
      return path_to_file;
    }
    inline char get_open_mode()
    {
      return open_mode;   
    }
    inline int get_line_pointer()
    {
      return line_pointer;  
    }
    void read_from_file();
    void write_to_file();
};

