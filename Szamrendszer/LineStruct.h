/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Szamrendszer
*
*****************************************************

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.10.26
*
* @description  Implementation of LineStruct class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#pragma once

//include libraries
#include <iostream>

//implementation of the class
class LineStruct
{
    private:
      int base_from;
      int base_to;
      std::string number;
      
    public:
      LineStruct()
      {
        base_from = 0;
        base_to   = 0;
        number    = "";
      }  
      LineStruct(int m_base_from, int m_base_to, std::string m_number)
      {
        base_from = m_base_from;
        base_to   = m_base_to;
        number    = m_number;
      }
      int get_base_from()
      {
        return base_from;
      }
      int get_base_to()
      {
        return base_to;
      }
      std::string get_number()
      {
        return number;
      }
      void write_line()
      { 
        std::cout << "Number : " << number << std::endl;
        std::cout << "Base from : " << base_from << std::endl;
        std::cout << "Base to : " << base_to << std::endl;
      }
};

      
             
            
    