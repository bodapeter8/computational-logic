/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Szamrendszer
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.10.26
*
* @description  Implementation of FileWrite class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#define MAX_LINE_NUMBER 100

//include libraries
#include <iostream>
#include <iomanip>
#include <fstream>
#include "LineStruct.h"
#include "Conversion.h"


class FileWrite{
    
    private:
      
      std::string file_path;
    
    public:
      FileWrite(std::string m_file_path)
      {
        file_path = m_file_path;
      }
      void write_to_file(LineStruct* m_LineStruct[MAX_LINE_NUMBER], int m_number_of_elements)
      {
        std::ofstream output_file;
        output_file.open(file_path);
        Conversion* converter[m_number_of_elements];
        
        std::cout << std::setw(10) << "OLD_BASE" << std::setw(15) << "OLD_NUMBER" << std::setw(5) << " " << std::setw(10) << "NEW_BASE"
        << std::setw(15) << "NEW_NUMBER" << "\n";

        output_file << std::setw(10) << "OLD_BASE" << std::setw(15) << "OLD_NUMBER" << std::setw(5) << " " << std::setw(10) << "NEW_BASE"
        << std::setw(15) << "NEW_NUMBER" << "\n";
        
        
        for(int i = 0; i < m_number_of_elements; i++)
        {
            
            int t_base_from      = m_LineStruct[i]->get_base_from();
            int t_base_to        = m_LineStruct[i]->get_base_to();
            std::string t_number = m_LineStruct[i]->get_number();
            try
            {
               converter[i] = new Conversion(t_base_from, t_base_to, t_number);
            }
            catch(char const* s)
            {
              //writing error to command line
              std::cout << std::setw(10) << t_base_from << std::setw(15) << t_number << std::setw(5) << " " << std::setw(10) << t_base_to
              << std::setw(15) << s << "\n";

              //writing error to file
              output_file << std::setw(10) << t_base_from << std::setw(15) << t_number << std::setw(5) << " " << std::setw(10) << t_base_to
              << std::setw(15) << s << "\n";
               
               continue;
            }
            
            //writing to command line
            std::cout << std::setw(10) << t_base_from << std::setw(15) << t_number << std::setw(5) << " " << std::setw(10) << t_base_to
            << std::setw(15) << converter[i]->convert() << "\n";

            //writing to file
            output_file << std::setw(10) << t_base_from << std::setw(15) << t_number << std::setw(5) << " " << std::setw(10) << t_base_to
            << std::setw(15) << converter[i]->convert() << "\n";
        }

        output_file.close();
      }
};