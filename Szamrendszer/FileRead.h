/*****************************************************
*@nev         Boda Peter
*
*@azonosito   bpim1793
*
*@csoport     511
*
*@feladat     Szamrendszer
*
*****************************************************/

/*****************************************************
* 
* @author       Boda Peter
*               bodapeter8@gmail.com   
*
* @date         2018.10.26
*
* @description  Implementation of FileRead class.
*
* Copyright (C) Boda Peter - All Rights Reserved
*
*****************************************************/

#define MAX_LINE_NUMBER 100

//include libraries
#include <iostream>
#include <fstream>
#include "LineStruct.h"

//implementation of the class
class FileRead
{
    private:
      std::string file_path;
      int number_of_lines;
      
    public:
      
      int get_number_of_lines()
      {
        //std::cout << number_of_lines;
        return number_of_lines;
      }


      LineStruct* m_LineStruct[MAX_LINE_NUMBER];
      FileRead(std::string m_file_path)
      {
        file_path = m_file_path;
        number_of_lines = 0;
      }
      void read_from_file()
      {
        std::ifstream input_file;
        input_file.open(file_path);
        
        int line_number = 0;
        int t_base_from;
        int t_base_to;
        std::string t_number;
        
        while(input_file)
        {
          input_file >> t_base_from;
          input_file >> t_base_to;
          input_file >> t_number;
          m_LineStruct[line_number] = new LineStruct(t_base_from, t_base_to, t_number);
          line_number++;
        }

        number_of_lines = line_number - 1;
        input_file.close();
      }
      ~FileRead()
      {
          
      }
};      
      