#pragma once
#include <iostream>

class NagySzam{
  private:
    int elojel;
    int szam_hossza;
    int* szamjegyek;
  public:
    NagySzam();
    NagySzam(const NagySzam &x);
    NagySzam(std::string szam);
    NagySzam& operator=(const NagySzam&x)
    {
		elojel       = x.elojel;
        szam_hossza  = x.szam_hossza;
        szamjegyek   = new int[szam_hossza];

      for(int i = 0 ; i < szam_hossza ; i++)
      {
        szamjegyek[i] = x.szamjegyek[i];
      }   
	  return *this;
    }
    inline int get_elojel(){
        return elojel;
    }
    inline int get_szam_hossza(){
        return szam_hossza;
    }
    inline int* get_szamjegyek(){
        return szamjegyek;
    }
};